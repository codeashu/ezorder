import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    Dimensions,
    Button,
    TextInput,
  } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
var {height, width} = Dimensions.get('window');

class LogoTitle extends React.Component {
    render() {
      return (
        <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
           <Text style={{fontSize:24,color:'#2f27af',textAlign:'center'}}>Add Item </Text>
        </View>
      );
    }
}
export default class AddNewItem extends React.Component {
    constructor(){
        super()
        this.state = {
            text:''
        }
    }
    static navigationOptions = {
        headerTitle: <LogoTitle />,
        headerTintColor: '#2f27af',
    };
  render() {
    return (
    <ScrollView>
      <View style={{flex:1,margin:10}}>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <Button
                onPress={()=>{}}
                title="Browse Catalog"
                color="#2f27af"
                />
            <Button
                onPress={()=>{}}
                title="Review Order"
                color="#2f27af"
                accessibilityLabel="Learn more about this purple button"
                />
            <Button
                onPress={()=>{}}
                title="Delete Item"
                color="#2f27af"
                accessibilityLabel="Learn more about this purple button"
                />
          </View>
          <View style={{margin:20, flexDirection:'row'}}>
              <View>
                <Image
                    source={require('../assets/images/product.png')}
                    style={{ width: 150, height: 150 }}
                />
              </View>
              <View >
                <View style={{flex:1, flexDirection:'row', width:width * 0.65 , marginLeft:30, marginTop:20 }}>
                    <Text style={{marginRight:15, marginTop:5, fontSize:20}}>Item:</Text>
                        <TextInput
                            style={{height: 40, width:width * 0.3, borderColor: 'gray', borderWidth: 1 , borderRadius:10, paddingLeft:10}}
                            placeholder='......'
                            onChangeText={(text) => this.setState({text})}
                            value={this.state.text}
                        />
                </View>
                    <View style={{margin:10, flexDirection:'row'}}>
                        <Text style={{fontSize:18}}>MOQ:</Text>
                        <Text style={{marginLeft:80,fontSize:18}}>REG:</Text>
                    </View>
                    <View style={{margin:10, flexDirection:'row'}}>
                        <Text style={{fontSize:18}}>Ship:</Text>
                        <Text style={{marginLeft:80,fontSize:18}}>Spec:</Text>
                    </View>
              </View>
          </View>
          <View style={{flexDirection:'row',justifyContent:'space-around', marginTop:20}}>
            <Button
                onPress={()=>{}}
                title="QTY : 1"
                color="#2f27af"
                style={{width:width*0.3}}
                />
            <Button
                onPress={()=>{}}
                title="EXT : "
                color="#2f27af"
                style={{width:width*0.3}}
                />
          </View>
          <View style ={{flexDirection:'row',justifyContent:'space-between', marginTop:40}}>
          <View style={{borderColor:'black', marginTop:10, justifyContent:'space-between'}}>
            <TouchableOpacity>
            <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>7</Text>
            </View>
            </TouchableOpacity>
            <View style={{margin:10}}/>
            <TouchableOpacity>
            <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>4</Text>
            </View>
            </TouchableOpacity>
            <View style={{margin:10}}/>
            <TouchableOpacity>
            <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>1</Text>
            </View>
            </TouchableOpacity>
            <View style={{margin:10}}/>
            <TouchableOpacity>
            <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                <Text style={{textAlign:'center', fontSize:16,fontWeight:'bold'}}>Scan Barcode</Text>
            </View>
            </TouchableOpacity>
          </View>
          <View style={{borderColor:'black', marginTop:10, justifyContent:'space-between'}}>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>8</Text>
                </View>
                </TouchableOpacity>
                <View style={{margin:10}}/>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>5</Text>
                </View>
                </TouchableOpacity>
                <View style={{margin:10}}/>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>2</Text>
                </View>
                </TouchableOpacity>
                <View style={{margin:10}}/>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>0</Text>
                </View>
                </TouchableOpacity>
          </View>
          <View style={{borderColor:'black', marginTop:10, justifyContent:'space-between'}}>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>9</Text>
                </View>
                </TouchableOpacity>
                <View style={{margin:10}}/>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>6</Text>
                </View>
                </TouchableOpacity>
                <View style={{margin:10}}/>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}>3</Text>
                </View>
                </TouchableOpacity>
                <View style={{margin:10}}/>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#D2D2D2',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:30,fontWeight:'bold'}}> > </Text>
                </View>
                </TouchableOpacity>
          </View>
          <View style={{borderColor:'black', marginTop:10, justifyContent:'space-between'}}>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#2f27af',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:35,fontWeight:'bold',color:'white'}}>+</Text>
                </View>
                </TouchableOpacity>
                <View style={{margin:8}}/>
                <TouchableOpacity>
                <View style={{height:height * 0.095,width:width * 0.17,backgroundColor:'#2f27af',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:40,fontWeight:'bold',color:'white'}}>-</Text>
                </View>
                </TouchableOpacity>
                <View style={{margin:8}}/>
                <TouchableOpacity>
                <View style={{height:height * 0.199,width:width * 0.17,backgroundColor:'#2f27af',elevation:5,justifyContent:'center',alignContent:'center'}}>
                    <Text style={{textAlign:'center', fontSize:20,fontWeight:'bold',color:'white'}}>ENTER</Text>
                </View>
                </TouchableOpacity>
          </View>
          </View>
      </View>
    </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   
  },
});
