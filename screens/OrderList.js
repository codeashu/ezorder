import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Dimensions,
  TextInput
} from 'react-native';

const data = [
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/M63H24W7JF0-L302-ALTGHOST?wid=1500&fmt=jpeg&qlt=80&op_sharpen=0&op_usm=1.0,1.0,5,0&iccEmbed=0",
			"name": "CHECK PRINT SHIRT",
			"price": 110
		},
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/FLGLO4FAL12-BEIBR?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
			"name": "GLORIA HIGH LOGO SNEAKER",
			"price": 91
		},
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/HWVG6216060-TAN?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
			"name": "CATE RIGID BAG",
			"price": 94.5
		},
		{
			"imgUrl": "http://guesseu.scene7.com/is/image/GuessEU/WC0001FMSWC-G5?wid=520&fmt=jpeg&qlt=80&op_sharpen=0&op_usm=1.0,1.0,5,0&iccEmbed=0",
			"name": "GUESS CONNECT WATCH",
			"price": 438.9
		},
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/AW6308VIS03-SAP?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
			"name": "'70s RETRO GLAM KEFIAH",
			"price": 20
    },
    {
			"imgUrl": "http://guesseu.scene7.com/is/image/GuessEU/WC0001FMSWC-G5?wid=520&fmt=jpeg&qlt=80&op_sharpen=0&op_usm=1.0,1.0,5,0&iccEmbed=0",
			"name": "GUESS CONNECT WATCH",
			"price": 438.9
		},
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/AW6308VIS03-SAP?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
			"name": "70s RETRO GLAM KEFIAH",
			"price": 20
    },
    {
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/FLGLO4FAL12-BEIBR?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
			"name": "GLORIA HIGH LOGO SNEAKER",
			"price": 91
		},
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/HWVG6216060-TAN?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
			"name": "CATE RIGID BAG",
			"price": 94.5
		},
];

class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
         <Text style={{fontSize:24,color:'#2f27af',textAlign:'center'}}>Product Catalog</Text>
      </View>
    );
  }
}

export default class OrderList extends React.Component {
  constructor(props){
    super()
    this.state = {
      text:''
    }
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerTintColor: '#2f27af',
  };
  renderHeader = () => {
    return <TextInput
    style={{height: 55, borderColor: 'gray', borderWidth: 2 , borderRadius:20, marginTop:10, marginLeft:5, marginRight:5, paddingLeft:10}}
    placeholder='What you are looking for ?'
    onChangeText={(text) => this.setState({text})}
    value={this.state.text}
  />;
  };
  render() {
    return (
      <View style={styles.container}>
          <FlatList
            data={data}
            ListHeaderComponent={this.renderHeader}
            renderItem={({item}) => 
              <View style={{height:120, backgroundColor:'#2f27af', marginTop:10, flexDirection:'row'}}>
                <View style={{margin:3}} >
                  <Image
                    source={{uri:item.imgUrl}}
                    style={{ width: 118, height: 112 }}
                  />
                </View>
                <View style={{justifyContent:'center',alignContent:'center', marginLeft:10}}>
                  <Text style={{fontSize:18,color:'white',fontWeight:'bold'}}>{item.name}</Text>
                  <Text style={{fontSize:16,color:'white', marginTop:25}}>{item.price}</Text>
                </View>
              </View>
            }
          />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
});
