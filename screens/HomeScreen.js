import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Dimensions
} from 'react-native';

const data = [
  {id: require('../assets/images/new.png') , value: 'New Order'},
  {id: require('../assets/images/current.png'), value: 'Current Order'},
  {id: require('../assets/images/list.png'), value: 'Order List'},
  {id: require('../assets/images/sales.png'), value: 'Sales Total'},
  {id: require('../assets/images/help.png'), value: 'Help'},
  {id: require('../assets/images/info.png'), value: 'App Info'},
];
const numColumns = 2;
const size = Dimensions.get('window').width/2;

class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
        <Image
          source={require('../assets/images/icon.png')}
          style={{ width: 45, height: 45 }}
         />
         <Text style={{fontSize:44,color:'#2f27af',textAlign:'center', marginTop:8}}>EZOrder</Text>
      </View>
    );
  }
}

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {height: 80}, 
  };
  onSelectItem = (item,index) => {
    if(item.value == "Order List"){
      this.props.navigation.navigate('Order')
    }else if (item.value == "New Order"){
      this.props.navigation.navigate('Add')
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={data}
          renderItem={({item,index}) => (
            <TouchableOpacity onPress={()=>{this.onSelectItem(item,index)}}>
              <View style={styles.itemContainer}>
                <View style={styles.item}>
                  <Image
                    source={item.id}
                    style={[index === 2 || index === 4 ? {width:100,height:120}:{ width:120,height:120 }]}
                  />
                  <Text style={{fontSize:18,color:'#2f27af',marginTop:index == 5 || index == 3 ? 17 : 15 , marginLeft: index == 0 ? 12 :0}}>
                    {item.value}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={item => item.id}
          numColumns={numColumns} />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  itemContainer: {
    width: size,
    height: size+20,
    backgroundColor:'#2f27af'
  },
  item: {
    flex: 1,
    marginLeft: 1.5,
    marginRight:1.5,
    marginBottom:3,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'white'
    
  }
});
