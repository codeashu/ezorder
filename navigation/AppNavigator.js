import React from 'react';
import { createStackNavigator } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import OrderList from '../screens/OrderList';
import AddNewItem from '../screens/AddNewItem';

import MainTabNavigator from './MainTabNavigator';

export default createStackNavigator({
  Main: HomeScreen,
  Order: OrderList,
  Add: AddNewItem,
},{
  headerLayoutPreset: 'center'
});

